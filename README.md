**REQUISITOS FUNCIONAIS**

O exercício proposto é a criação de um sistema simples para controle de ponto de uma
empresa. O sistema deve permitir o cadastro de usuários e o registro de ponto dos mesmos.

Operações possíveis para funcionário:

[Criação](#post-request): todos os atributos devem ser preenchidos, com exceção do id, que será gerado
automaticamente no momento do cadastro.

[Edição](#put-request): todos os campos são editáveis, com exceção do id e da data de cadastro.

[Consulta](#get-id-request): deve-se exibir os dados de um usuário de acordo com id informado.

[Listagem](#get-request): deve ser feita a listagem de todos os usuários cadastrados na base.


#FUNCIONARIO
### POST Request
**ENDPOINT**: /funcionario

Faz o cadastro de funcionário no sistema.


```json
{
    "nome": "Usuario",
    "cpf": "56969997863",
    "email": "usuario@gmail.com"
}
```

RETORNO 201 CREATED
```json
{
    "id": 1,
    "nome": "Usuario",
    "cpf": "56969997863",
    "email": "usuario@gmail.com",
    "dataCadastro": "2020-07-06"
}
```


###PUT Request
**ENDPOINT**: /funcionario/{id} 

Faz a alteração do usuário. 

```json
{
        "id": 1,
        "nome": "Usuario1",
        "cpf": "56969997863",
        "email": "usuario1@gmail.com"
    }
```

RETORNO 200 OK
```json
{
    "id": 1,
    "nome": "Usuario1",
    "cpf": "56969997863",
    "email": "usuario1@gmail.com",
    "dataCadastro": "2020-07-06"
}
```


###GET Request
**ENDPOINT**: /funcionario
Exibe a lista de funcionários cadastrados no sistema.

RETORNO 200 OK
```json
{
    "id": 1,
    "nome": "Usuario",
    "cpf": "56969997863",
    "email": "usuario@gmail.com",
    "dataCadastro": "2020-07-06"
}
```


###GET ID Request
**ENDPOINT**: /funcionario/{id}

Faz a consulta de acordo com o id informado.

RETORNO 200 ok
```json
{
    "id": 1,
    "nome": "Usuario",
    "cpf": "56969997863",
    "email": "usuario@gmail.com",
    "dataCadastro": "2020-07-06"
}
```

#PONTO
Operações possíveis para batidas de ponto:

[Criação](#post-ponto-request): cadastro uma batida de ponto (seja entrada ou saída) para um usuário específico, de
acordo com o id informado.

[Listagem](#get-ponto-request): listagem de todas as batidas de ponto de um único usuário, de acordo com o id
informado. Deve-se mostrar na resposta, além da lista de batidas, o total de horas trabalhadas
por esse usuário.

###POST Ponto Request
**ENDPOINT**: /ponto

Faz a batida de ponto de acordo com o id do funcionario, passando apenas o tipo de batida. A data hora batida é definida
automáticamente.

**PONTO DE ENTRADA**
```json
{
    "tipoBatidaEnum" : "ENTRADA",
    "funcionario" : 
                {
                    "id": "1"
                }
}
```

RETORNO 201 Created
```json
{
    "id": 1,
    "dataHoraBatida": "2020-07-06 08:48:46",
    "tipoBatidaEnum": "ENTRADA",
    "funcionario": {
        "id": 1,
        "nome": "Usuario1",
        "cpf": "56969997863",
        "email": "usuario@gmail.com",
        "dataCadastro": "2020-07-06"
    }
}
```

**PONTO DE SAÍDA**
```json
{
    "tipoBatidaEnum" : "SAIDA",
    "funcionario" : 
                {
                    "id": "1"
                }
}

```

RETORNO 201 Created
```json
{
    "id": 2,
    "dataHoraBatida": "2020-07-06 08:49:32",
    "tipoBatidaEnum": "SAIDA",
    "funcionario": {
        "id": 1,
        "nome": "Usuario1",
        "cpf": "56969997863",
        "email": "usuario@gmail.com",
        "dataCadastro": "2020-07-06"
    }
}
```

###GET Ponto Request
**ENDPOINT**: /ponto/{id}

Faz a pesquisa de registros de ponto de acordo com o id do funcionário.

RETORNO 200 OK
```json
{
    "registros": [
        {
            "id": 1,
            "dataHoraBatida": "2020-07-06 08:48:46",
            "tipoBatidaEnum": "ENTRADA",
            "funcionario": {
                "id": 1,
                "nome": "Usuario1",
                "cpf": "56969997863",
                "email": "usuario@gmail.com",
                "dataCadastro": "2020-07-06"
            }
        },
        {
            "id": 2,
            "dataHoraBatida": "2020-07-06 08:49:32",
            "tipoBatidaEnum": "SAIDA",
            "funcionario": {
                "id": 1,
                "nome": "Usuario1",
                "cpf": "56969997863",
                "email": "usuario@gmail.com",
                "dataCadastro": "2020-07-06"
            }
        }
    ],
    "horasTrabalhadas": "00:00:46"
}
```