package br.com.apipontoeletronico.controllers;

import br.com.apipontoeletronico.enums.TipoBatidaEnum;
import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.models.Ponto;
import br.com.apipontoeletronico.models.dtos.RespostaPontoDTO;
import br.com.apipontoeletronico.services.FuncionarioService;
import br.com.apipontoeletronico.services.PontoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PontoController.class)
public class PontoControllerTest {

    @MockBean
    private PontoService pontoService;

    @MockBean
    private FuncionarioService funcionarioService;

    @Autowired
    private MockMvc mockMvc;

    Funcionario funcionario;
    Ponto ponto;
    List<Ponto> registros;
    RespostaPontoDTO respostaPontoDTO;

    @BeforeEach
    public void setUp(){
        funcionario = new Funcionario();
        funcionario.setId((long)1);
        funcionario.setNome("Usuario");
        funcionario.setCpf("56969997863");
        funcionario.setEmail("usuario@gmail.com");

        ponto = new Ponto();
        ponto.setFuncionario(funcionario);
        ponto.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);

        registros = new ArrayList<>();
        registros.add(ponto);
//
        respostaPontoDTO = new RespostaPontoDTO(registros, "10:02");

    }

    @Test
    public void testarRegistrarPontoComSucesso() throws Exception {

        Mockito.when(pontoService.incluirRegistroPonto(Mockito.any(Ponto.class))).then(pontoObjeto -> {
            ponto.setId(1);
            ponto.setDataHoraBatida(LocalDateTime.now());
            return ponto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(ponto);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarPorFuncionario() throws Exception {
        funcionario.setId((long)1);
        Mockito.when(pontoService.listarPontoPorFuncionario(Mockito.anyLong())).thenReturn(respostaPontoDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }


}
