package br.com.apipontoeletronico.controllers;

import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.services.FuncionarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

@WebMvcTest(FuncionarioController.class)
public class FuncionarioControllerTest {

    @MockBean
    private FuncionarioService funcionarioService;

    @Autowired
    private MockMvc mockMvc;

    Funcionario funcionario;

    @BeforeEach
    public void setUp(){
        funcionario = new Funcionario();
        funcionario.setNome("Usuario");
        funcionario.setCpf("56969997863");
        funcionario.setEmail("usuario@gmail.com");
    }

    @Test
    public void testarCriarUsuario() throws Exception {
        Mockito.when(funcionarioService.salvarFuncionario(Mockito.any(Funcionario.class))).then(funcionarioObjeto -> {
            funcionario.setId((long)1);
            funcionario.setDataCadastro(LocalDate.now());
            return funcionario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(funcionario);

        mockMvc.perform(MockMvcRequestBuilders.post("/funcionario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo((LocalDate.now().toString()))));
    }

    @Test
    public void testarAtualizarFuncionarioComSucesso() throws Exception {
        funcionario.setId((long)1);
        funcionario.setNome("Usuario2");
        Mockito.when(funcionarioService.atualizarFuncionario(Mockito.anyLong(), Mockito.any(Funcionario.class)))
                .thenReturn(funcionario);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(funcionario);

        mockMvc.perform(MockMvcRequestBuilders.put("/funcionario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void testarBuscarPorIdComSucesso() throws Exception {
        funcionario.setId((long)1);
        Mockito.when(funcionarioService.buscarPorId(Mockito.anyLong())).thenReturn(funcionario);

        mockMvc.perform(MockMvcRequestBuilders.get("/funcionario/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarExibirTodosOsFuncionarios() throws Exception {
        Iterable<Funcionario> funcionarios = Arrays.asList(funcionario);

        Mockito.when(funcionarioService.findAll()).thenReturn(funcionarios);

        mockMvc.perform(MockMvcRequestBuilders.get("/funcionario")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }




}
