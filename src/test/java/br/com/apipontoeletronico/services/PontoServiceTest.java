package br.com.apipontoeletronico.services;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.apipontoeletronico.enums.TipoBatidaEnum;
import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.models.Ponto;
import br.com.apipontoeletronico.repositories.FuncionarioRepository;
import br.com.apipontoeletronico.repositories.PontoRepository;

@SpringBootTest
public class PontoServiceTest {

    @Autowired
    private PontoService pontoService;

    @MockBean
    PontoRepository pontoRepository;

    @MockBean
    FuncionarioRepository funcionarioRepository;

    Funcionario funcionario;

    Ponto ponto;

    @BeforeEach
    public void setup() {
        funcionario = new Funcionario();
        funcionario.setId((long) 1);
        funcionario.setNome("Andressa");
        funcionario.setEmail("andressa@itau.com.br");
        funcionario.setCpf("12345678911");
        funcionario.setDataCadastro(LocalDate.now());

        ponto = new Ponto();

        pontoService = new PontoService();
        pontoService.funcionarioService = Mockito.mock(FuncionarioService.class);

    }


    @Test
    public void testarInclusaoPonto() {

        long id = 1;

        Mockito.when(pontoService.funcionarioService.buscarPorId(id)).thenReturn(funcionario);

        Ponto pontoTeste = new Ponto();

        pontoTeste.setFuncionario(funcionario);
        pontoTeste.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);
        pontoTeste.setId(id);
        pontoTeste.setDataHoraBatida(LocalDateTime.now());

        Mockito.when(pontoRepository.save(pontoTeste)).thenReturn(pontoTeste);

        Assertions.assertEquals(id, pontoTeste.getId());

    }

    @Test
    public void testarConversaoHoraMinuto() {

        String horaTest = pontoService.converterHora_minuto((long) 10);

        String format = "00:00:10";

        Assertions.assertEquals(horaTest, format);

    }
}