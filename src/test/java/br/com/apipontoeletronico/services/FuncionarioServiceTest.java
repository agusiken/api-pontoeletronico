package br.com.apipontoeletronico.services;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.repositories.FuncionarioRepository;

@SpringBootTest
public class FuncionarioServiceTest {

    @Autowired
    private FuncionarioService funcionarioService;

    @MockBean
    FuncionarioRepository funcionarioRepository;

    Funcionario funcionario;

    @BeforeEach
    public void setup() {
        funcionario = new Funcionario();
        funcionario.setId((long) 1);
        funcionario.setNome("Andressa");
        funcionario.setEmail("andressa@itau.com.br");
        funcionario.setCpf("12345678911");
        funcionario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarSalvarFuncionario() {

        Funcionario funcionarioTeste = new Funcionario();
        funcionarioTeste.setNome("Andressa");
        funcionarioTeste.setEmail("andressa@hotmail.com");
        funcionarioTeste.setCpf("12345678911");

        Mockito.when(funcionarioRepository.save(funcionarioTeste)).then(leadLamb -> {
                    //simula o preenchimento do id como se fosse o banco de dados;
                    funcionarioTeste.setId((long) 1);
                    return funcionarioTeste;
                }
        );

        Funcionario funcionarioObjeto = funcionarioService.salvarFuncionario(funcionarioTeste);

        Assertions.assertEquals(LocalDate.now(), funcionarioObjeto.getDataCadastro());
        Assertions.assertEquals(1, funcionarioTeste.getId());

    }

    @Test
    public void testarAtualizarFuncionario() {

        long id = 1;

        Mockito.when(funcionarioRepository.existsById(id)).thenReturn(true);

        funcionario.setId(id);

        Mockito.when(funcionarioRepository.findById(id)).thenReturn(Optional.of(funcionario));

        Funcionario funcionarioTeste = new Funcionario();
        funcionarioTeste.setId((long) 1);
        funcionarioTeste.setNome("Ayumi");
        funcionarioTeste.setDataCadastro(LocalDate.now());
        funcionarioTeste.setEmail("ayumi@com.br");

        Mockito.when(funcionarioRepository.save(funcionarioTeste)).thenReturn(funcionarioTeste);
        Funcionario funcionarioAtualizado = funcionarioService.atualizarFuncionario((long) 1, funcionarioTeste);

        Assertions.assertEquals(funcionarioAtualizado, funcionarioTeste);
    }


    @Test
    public void testarBuscarPorTodosOsFuncionarios() {
        Iterable<Funcionario> funcionarios = Arrays.asList(funcionario);
        Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);

        Iterable<Funcionario> funcionarioIterable = funcionarioService.findAll();

        Assertions.assertEquals(funcionarios, funcionarioIterable);
    }


    @Test
    public void testarBuscarUmFuncionarioPorId() {
        long id = 1;
        funcionario.setId(id);

        Mockito.when(funcionarioRepository.findById(id)).thenReturn(Optional.of(funcionario));

        Funcionario func = funcionarioService.buscarPorId(id);

        Assertions.assertEquals(funcionario, func);
    }


}
