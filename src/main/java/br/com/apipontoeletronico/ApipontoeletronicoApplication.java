package br.com.apipontoeletronico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApipontoeletronicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApipontoeletronicoApplication.class, args);
	}

}
