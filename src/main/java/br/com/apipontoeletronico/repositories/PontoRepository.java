package br.com.apipontoeletronico.repositories;

import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.models.Ponto;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Long> {
    Iterable<Ponto> findAllByFuncionario(Funcionario funcionario);
}
