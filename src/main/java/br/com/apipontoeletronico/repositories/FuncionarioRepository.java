package br.com.apipontoeletronico.repositories;

import br.com.apipontoeletronico.models.Funcionario;
import org.springframework.data.repository.CrudRepository;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {
    Funcionario findByEmail(String email);

    Funcionario findBycpf(String cpf);
}
