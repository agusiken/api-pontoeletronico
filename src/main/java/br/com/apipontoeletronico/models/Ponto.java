package br.com.apipontoeletronico.models;

import br.com.apipontoeletronico.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@JsonIgnoreProperties(value = "dataHoraBatida", allowGetters = true)
public class Ponto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dataHoraBatida;

    @JsonFormat(shape= JsonFormat.Shape.STRING)
    @NotNull(message = "Tipo de batida obrigatória")
    private TipoBatidaEnum tipoBatidaEnum;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "Necessita de usuário")
    private Funcionario funcionario;

    public Ponto() {
    }

    public Ponto(long id, LocalDateTime dataHoraBatida, @NotNull(message = "Tipo de batida obrigatória") TipoBatidaEnum tipoBatidaEnum, @NotNull(message = "Necessita de usuário") Funcionario funcionario) {
        this.id = id;
        this.dataHoraBatida = dataHoraBatida;
        this.tipoBatidaEnum = tipoBatidaEnum;
        this.funcionario = funcionario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatidaEnum() {
        return tipoBatidaEnum;
    }

    public void setTipoBatidaEnum(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatidaEnum = tipoBatidaEnum;
    }
}
