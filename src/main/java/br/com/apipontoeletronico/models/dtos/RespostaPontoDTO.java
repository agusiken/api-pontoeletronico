package br.com.apipontoeletronico.models.dtos;

import br.com.apipontoeletronico.models.Ponto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RespostaPontoDTO {


    private List<Ponto> registros = new ArrayList<>();

    private String horasTrabalhadas;

    public RespostaPontoDTO() {
    }


    public RespostaPontoDTO(List<Ponto> registros, String horasTrabalhadas) {
        this.registros = registros;
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public List<Ponto> getRegistros() {
        return registros;
    }

    public void setRegistros(List<Ponto> registros) {
        this.registros = registros;
    }

    public String getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(String horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
}
