package br.com.apipontoeletronico.services;

import br.com.apipontoeletronico.enums.TipoBatidaEnum;
import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.models.Ponto;
import br.com.apipontoeletronico.models.dtos.RespostaPontoDTO;
import br.com.apipontoeletronico.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PontoService {

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    FuncionarioService funcionarioService;

    public Ponto incluirRegistroPonto(Ponto ponto){
        LocalDateTime dataHora = LocalDateTime.now();

        Funcionario funcionario = funcionarioService.buscarPorId(ponto.getFuncionario().getId());
        ponto.setFuncionario(funcionario);
        ponto.setDataHoraBatida(dataHora);
        ponto.setTipoBatidaEnum(ponto.getTipoBatidaEnum());

        Ponto pontoObjeto = pontoRepository.save(ponto);
        return pontoObjeto;
    }

    public RespostaPontoDTO listarPontoPorFuncionario(Long id){


        Funcionario funcionario = funcionarioService.buscarPorId(id);

        Iterable<Ponto> pontoObjeto = pontoRepository.findAllByFuncionario(funcionario);

        RespostaPontoDTO respostaPontoDTO = new RespostaPontoDTO();
        List<Ponto> pontoRegistros = new ArrayList<>();

        LocalDateTime entrada = null;
        LocalDateTime saida = null;

        long quantidadeHoras = 0;

        for(Ponto ponto: pontoObjeto){
            pontoRegistros.add(ponto);

            if (ponto.getTipoBatidaEnum().equals(TipoBatidaEnum.ENTRADA)) {
                entrada = ponto.getDataHoraBatida();

            }
            if(ponto.getTipoBatidaEnum().equals(TipoBatidaEnum.SAIDA)) {
                saida = ponto.getDataHoraBatida();

            }



            if (entrada != null && saida != null) {
                Duration duracao = Duration.between(entrada, saida);
                quantidadeHoras += duracao.getSeconds();
                entrada = null;
                saida = null;
            }
        }

        respostaPontoDTO = new RespostaPontoDTO(pontoRegistros, converterHora_minuto(quantidadeHoras));

        return respostaPontoDTO;

    }

    public String converterHora_minuto(long quantidadeHoras) {
        String resultado = String.format("%02d:%02d:%02d", quantidadeHoras / 3600000, (quantidadeHoras % 3600000) / 60, quantidadeHoras);

        return resultado;
    }
}
