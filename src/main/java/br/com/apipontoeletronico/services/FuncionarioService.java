package br.com.apipontoeletronico.services;

import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.repositories.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    public Funcionario salvarFuncionario(Funcionario funcionario) {

        funcionario.setDataCadastro(LocalDate.now());
        return funcionarioRepository.save(funcionario);

    }

    public Funcionario atualizarFuncionario(Long id, Funcionario funcionario) {
        if (funcionarioRepository.existsById(id)) {
            funcionario.setId(id);

            Funcionario funcionarioObjeto = buscarPorId(id);

//        if(pessoaRepository.existsById(id)){
            funcionario.setCpf(funcionario.getCpf());
            funcionario.setEmail(funcionario.getEmail());
            funcionario.setNome(funcionario.getNome());
            funcionario.setDataCadastro(funcionarioObjeto.getDataCadastro());

            return funcionarioRepository.save(funcionario);
        }
        throw new RuntimeException("Usuario não encontrado");
    }

    public Funcionario buscarPorId(Long id) {
        Optional<Funcionario> optionalFuncionario = funcionarioRepository.findById(id);

        if (optionalFuncionario.isPresent()) {
            return optionalFuncionario.get();
        }

        throw new RuntimeException("A pessoa não foi encontrada!");
    }


    public Iterable<Funcionario> findAll() {
        try {
            Iterable<Funcionario> funcionarios = funcionarioRepository.findAll();
            return funcionarios;
        } catch (RuntimeException exception) {
            throw new RuntimeException("Base de dados vazia. Sem usuários cadastrados.");
        }
    }
}
