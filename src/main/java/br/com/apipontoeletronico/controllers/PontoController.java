package br.com.apipontoeletronico.controllers;

import br.com.apipontoeletronico.models.Ponto;
import br.com.apipontoeletronico.models.dtos.RespostaPontoDTO;
import br.com.apipontoeletronico.services.PontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.xml.ws.Response;

@RestController
@RequestMapping("/ponto")
public class PontoController {

    @Autowired
    private PontoService pontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto registrarPonto(@RequestBody @Valid Ponto ponto) {

        Ponto pontoObjeto = pontoService.incluirRegistroPonto(ponto);
        return pontoObjeto;

    }

    @GetMapping("/{id}")
    public RespostaPontoDTO listarMarcacoes(@PathVariable(name = "id") Long id) {
        try {
            RespostaPontoDTO respostaPontoDTO = pontoService.listarPontoPorFuncionario(id);
            return respostaPontoDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }


}
