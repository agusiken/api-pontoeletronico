package br.com.apipontoeletronico.controllers;

import br.com.apipontoeletronico.models.Funcionario;
import br.com.apipontoeletronico.services.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {

    @Autowired
    private FuncionarioService funcionarioService;

    @PostMapping
    public ResponseEntity<Funcionario> cadastrarFuncionario(@RequestBody @Valid Funcionario funcionario) {

        Funcionario funcionarioObjeto = funcionarioService.salvarFuncionario(funcionario);
        return ResponseEntity.status(201).body(funcionarioObjeto);

    }

    @PutMapping("/{id}")
    public Funcionario atualizarFuncionario(@PathVariable(name = "id") Long id, @RequestBody Funcionario funcionario) {
        try {
            Funcionario funcionarioObjeto = funcionarioService.atualizarFuncionario(id, funcionario);
            return funcionarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Funcionario> exibirTodas() {
        Iterable<Funcionario> funcionarioObjeto = funcionarioService.findAll();
        return funcionarioObjeto;

    }

    @GetMapping("/{id}")
    public Funcionario buscarPorId(@PathVariable(name = "id") Long id) {
        try {
            Funcionario funcionario = funcionarioService.buscarPorId(id);
            return funcionario;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
